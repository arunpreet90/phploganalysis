import re
from operator import itemgetter
from collections import OrderedDict
import sys


date_pattern = re.compile(r'\d{4}-\d{2}-\d{2}')


def is_valid_log_record(log_line):
    if not log_line.startswith("["):
        return False

    if not "ERROR" in log_line:
        return  False

    start_bracket = log_line.find("[")
    end_bracket = log_line.find("]")

    if start_bracket == -1 or end_bracket == -1:
        return False

    timestamp = log_line[start_bracket+1:end_bracket]

    if date_pattern.search(timestamp):
        return True

    return False


def do_log_analysis(filename):

    fil = open(filename)

    errors = dict()

    for line in fil.readlines():

        if not is_valid_log_record(log_line=line):
            continue

        line_parts = line.split(":")
        error_description = line_parts[3]
        error_msg = line[line.find(line_parts[4]):]

        if error_description not in errors:
            errors[error_description] = dict()
            errors[error_description]["counter"] = 0
            errors[error_description]["err_msgs"] = dict()

        errors[error_description]["counter"] = errors[error_description]["counter"] + 1
        if error_msg not in errors[error_description]["err_msgs"]:
            errors[error_description]["err_msgs"][error_msg] = 0
        errors[error_description]["err_msgs"][error_msg] = errors[error_description]["err_msgs"][error_msg] + 1

    print "Total unique errors :%d" % len(errors)

    for err_type, err_data in errors.iteritems():

        print "\n -----------------------Base Error Type :%s----------------------" % err_type
        print "\n Base Error Count :%d" % err_data["counter"]

        print "\n---sub type errors--- \n"

        err_msgs = err_data["err_msgs"]

        err_msgs = OrderedDict(sorted(err_msgs.items(), key=itemgetter(1), reverse=True))

        for err_msg, counter in err_msgs.iteritems():
            print "Count :%d , Error :%s" % (counter,  err_msg)


# handle command line
arguments = sys.argv[1:]

if len(arguments) != 1:
    print "Usage python main.py  filename"
    sys.exit(1)

do_log_analysis(arguments[0])